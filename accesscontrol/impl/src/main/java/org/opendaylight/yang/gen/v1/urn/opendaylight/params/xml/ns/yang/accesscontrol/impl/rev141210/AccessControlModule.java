/*
 * Copyright © 2015 Copyright (c) 2015 Zachary Podbela, Craig Boswell and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.impl.rev141210;

import edu.duke.accesscontrol.impl.AccessControlProvider;

public class AccessControlModule extends org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.impl.rev141210.AbstractAccessControlModule {
    public AccessControlModule(org.opendaylight.controller.config.api.ModuleIdentifier identifier, org.opendaylight.controller.config.api.DependencyResolver dependencyResolver) {
        super(identifier, dependencyResolver);
    }

    public AccessControlModule(org.opendaylight.controller.config.api.ModuleIdentifier identifier, org.opendaylight.controller.config.api.DependencyResolver dependencyResolver, org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.impl.rev141210.AccessControlModule oldModule, java.lang.AutoCloseable oldInstance) {
        super(identifier, dependencyResolver, oldModule, oldInstance);
    }

    @Override
    public void customValidation() {
        // add custom validation form module attributes here.
    }

    @Override
    public java.lang.AutoCloseable createInstance() {
        AccessControlProvider provider = new AccessControlProvider();
        getBrokerDependency().registerProvider(provider);
        return provider;
    }

}
