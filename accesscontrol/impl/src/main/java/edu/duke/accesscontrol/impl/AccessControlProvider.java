/*
 * Copyright © 2015 Copyright (c) 2015 Zachary Podbela, Craig Boswell and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package edu.duke.accesscontrol.impl;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.sal.binding.api.BindingAwareBroker.ProviderContext;
import org.opendaylight.controller.sal.binding.api.BindingAwareBroker.RpcRegistration;
import org.opendaylight.controller.sal.binding.api.BindingAwareProvider;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.AccesscontrolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessControlProvider implements BindingAwareProvider, AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(AccessControlProvider.class);
    private RpcRegistration<AccesscontrolService> service;

    @Override
    public void onSessionInitiated(ProviderContext session) {
        LOG.info("AccessControlProvider Session Initiated");
        DataBroker db = session.getSALService(DataBroker.class);
        service = session.addRpcImplementation(AccesscontrolService.class, new AccessControlImpl(db));
    }

    @Override
    public void close() throws Exception {
        LOG.info("AccessControlProvider Closed");
        if (service != null) {
            service.close();
        }
    }

}
