/*
 * Copyright © 2015 Copyright (c) 2015 Zachary Podbela, Craig Boswell and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package edu.duke.accesscontrol.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadOnlyTransaction;
import org.opendaylight.controller.md.sal.binding.api.WriteTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.ReadFailedException;
import org.opendaylight.controller.md.sal.common.api.data.TransactionCommitFailedException;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.AccesscontrolService;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GetBundleIdInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GetBundleIdOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GetBundleIdOutputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GrantPrivilegeInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IsAuthorizedInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IsAuthorizedOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IsAuthorizedOutputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IssueTokenInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IssueTokenOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IssueTokenOutputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.PermissionMap;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.PermissionMapBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.ResetToDefaultInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RestTokenEntry;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RestTokenTable;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RestTokenTableBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RevokePrivilegeInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RevokeTokenInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.permission.map.PermissionMapEntry;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.permission.map.PermissionMapEntryBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.permission.map.PermissionMapEntryKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.permission.map.permission.map.entry.AccessEntry;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.permission.map.permission.map.entry.AccessEntryBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.permission.map.permission.map.entry.AccessEntryKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.rest.token.table.RestTokenEntryBid;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.rest.token.table.RestTokenEntryBidBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.rest.token.table.RestTokenEntryBidKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.rest.token.table.RestTokenEntryToken;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.rest.token.table.RestTokenEntryTokenBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.rest.token.table.RestTokenEntryTokenKey;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.opendaylight.yangtools.yang.common.RpcResultBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.CheckedFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;

public class AccessControlImpl implements AccesscontrolService {
	private static final String DEFAULT_STR = "*";
    private static final Logger LOG = LoggerFactory.getLogger(AccessControlImpl.class);
    private static final LogicalDatastoreType DS_TYPE = LogicalDatastoreType.CONFIGURATION;
    
    private DataBroker db;

    public AccessControlImpl(DataBroker db) {
    	this.db = db;
    	initializeDataTree();
    }
    
    //================================================================================
    // AccesscontrolService Methods
    //================================================================================
    
    // Enforcement API
    
    @Override
    public Future<RpcResult<IsAuthorizedOutput>> isAuthorized(IsAuthorizedInput input) {
    	IsAuthorizedOutputBuilder builder = new IsAuthorizedOutputBuilder();
    	PermissionLocation loc = (input != null) ? (new PermissionLocation(input)) : (new PermissionLocation());
    	boolean result = readAccess(loc);
    	builder.setResult(result);
    	return RpcResultBuilder.success(builder.build()).buildFuture();
    }
    
    // Permission Management API

	@Override
	public Future<RpcResult<Void>> revokePrivilege(RevokePrivilegeInput input) {
		PermissionLocation loc = (input != null) ? (new PermissionLocation(input)) : (new PermissionLocation());
		writeAccess(loc, false);
		return Futures.immediateFuture(RpcResultBuilder.<Void> success().build());
	}

	@Override
	public Future<RpcResult<Void>> resetToDefault(ResetToDefaultInput input) {    	
		if(input.getAppId() != null) deleteDefinedAccess(input.getAppId());
		return Futures.immediateFuture(RpcResultBuilder.<Void> success().build());
	}

	@Override
	public Future<RpcResult<Void>> grantPrivilege(GrantPrivilegeInput input) {
		PermissionLocation loc = (input != null) ? (new PermissionLocation(input)) : (new PermissionLocation());
		writeAccess(loc, true);
		return Futures.immediateFuture(RpcResultBuilder.<Void> success().build());
	}
	
    // Token Management API
	
	@Override
	public Future<RpcResult<Void>> revokeToken(RevokeTokenInput input) {
		if(input == null) return Futures.immediateFuture(RpcResultBuilder.<Void> failed().build());
		deleteRestTokenEntry(input.getAppId());
		return Futures.immediateFuture(RpcResultBuilder.<Void> success().build());
	}

	@Override
	public Future<RpcResult<GetBundleIdOutput>> getBundleId(GetBundleIdInput input) {
		if(input == null) return Futures.immediateFuture(RpcResultBuilder.<GetBundleIdOutput> failed().build());
		RestTokenEntry rte = readRestTokenEntry(null, input.getToken());
		GetBundleIdOutput output = new GetBundleIdOutputBuilder()
			.setAppId((rte != null) ? rte.getBundleId() : "")
			.build();
		return Futures.immediateFuture(RpcResultBuilder.success(output).build());
	}

	@Override
	public Future<RpcResult<IssueTokenOutput>> issueToken(IssueTokenInput input) {
		//Generate API Key
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			LOG.warn("Issue Token Failed. Missing MessageDigest MD5 instance. Cannot generate API Key.");
		}
		if(input == null || digest == null) return Futures.immediateFuture(RpcResultBuilder.<IssueTokenOutput> failed().build());
		Random r = new Random();
		int bLen = 30;
		byte[] nbyte = new byte[bLen];
		r.nextBytes(nbyte);
		digest.update(nbyte, 0, bLen);
		String md5 = new BigInteger(1, digest.digest()).toString(16);
		
		writeRestTokenEntry(input.getAppId(), md5);
		IssueTokenOutputBuilder builder = new IssueTokenOutputBuilder().setToken(md5);
		return Futures.immediateFuture(RpcResultBuilder.<IssueTokenOutput> success(builder.build()).build());
	}
	
    //================================================================================
    // Data Broker Helper Methods
    //================================================================================
	
	/**
	 * Initialize Data Tree by writing PermissionMap (top level entity)
	 */
	private void initializeDataTree() {
        LOG.debug("Attempting to initializing data tree with global default accessiblity.");
        WriteTransaction transaction = db.newReadWriteTransaction();
        
		// Write PermissionMap
		InstanceIdentifier<PermissionMap> iid = InstanceIdentifier.create(PermissionMap.class);
		PermissionMap pmap = new PermissionMapBuilder().build();
        transaction.put(DS_TYPE, iid, pmap);
        
        // Write RestTokenTable
		InstanceIdentifier<RestTokenTable> iid2 = InstanceIdentifier.create(RestTokenTable.class);
		RestTokenTable rtt = new RestTokenTableBuilder().build();
        transaction.put(DS_TYPE, iid2, rtt);
        
        submitAndLogWriteTransaction(transaction, "Data tree initialization: PermissionMap Put, RestTokenTable Put");
        
        //Add global default
        PermissionLocation loc = new PermissionLocation(null, null);
        writeAccess(loc, true);
    }
	
	/**
	 * Read a permission definition for the associated identifier and action, and return the value.
	 * If the identifier is null, default access is used. If both inputs are null, default accessibility is used.
	 */
	private boolean readAccess(PermissionLocation loc) {
		Queue<PermissionLocation> fallback = getFallbackLoctions(loc);
		
		while(loc != null) {
			InstanceIdentifier<AccessEntry> iid = generateAccessEntryInstanceIdentifier(loc);
			
			//Create Transaction and Run
			ReadOnlyTransaction transaction = db.newReadOnlyTransaction();
			CheckedFuture<Optional<AccessEntry>, ReadFailedException> future = transaction.read(DS_TYPE, iid);
			Optional<AccessEntry> optional = Optional.absent();
	        try {
	            optional = future.checkedGet();
	        } catch (ReadFailedException e) {
	            LOG.warn("Reading AccessEntry failed:",e);
	        }
	        
	        //Use result
	        if(optional.isPresent()) {
	        	AccessEntry entry = optional.get();
	        	return entry.isIsAccessible();
	        } else {
	        	//Access is undefined for given identifier/action combo, use a next location
	        	loc = fallback.poll();
	        }
		}
		
		LOG.warn("Line 141 Should not be reachable. Global default was not readable.");
		return false;
	}
	
	/**
	 * Write a permission definition for the associated identifier
	 */
	private void writeAccess(PermissionLocation loc, boolean isAccessible) {
		WriteTransaction transaction = db.newReadWriteTransaction();
		
		//Write PermissionMapEntry if it does not exist
		InstanceIdentifier<PermissionMapEntry> iid = generatePermissionMapEntryInstanceIdentifier(loc);
		PermissionMapEntry pmEntry = new PermissionMapEntryBuilder()
        	.setIdentifier(loc.getIdentifier())
        	.build();
        transaction.merge(DS_TYPE, iid, pmEntry);
		
		//Write AccessEntry
        InstanceIdentifier<AccessEntry> iid2 = generateAccessEntryInstanceIdentifier(loc);
        AccessEntry accessEntry = new AccessEntryBuilder()
        	.setNapiAction(loc.getAction())
        	.setIsAccessible(isAccessible)
        	.build();
        transaction.put(DS_TYPE, iid2, accessEntry);
        
        //Run
        submitAndLogWriteTransaction(transaction, "PermissionMapEntry Put, AccessEntry Put");
	}
	
	/**
	 * Delete the permissions associated with identifier so they fall back to the default permissions.
	 */
	private void deleteDefinedAccess(String identifier) {
		WriteTransaction transaction = db.newWriteOnlyTransaction();
		InstanceIdentifier<PermissionMapEntry> iid = generatePermissionMapEntryInstanceIdentifier(identifier);
		transaction.delete(DS_TYPE, iid);
        submitAndLogWriteTransaction(transaction, "PermissionMapEntry Delete", true);
	}
	
	/**
	 * Read the entry for a given bundleID or apiToken
	 */
	private RestTokenEntry readRestTokenEntry(String bundleID, String apiToken) {
		InstanceIdentifier<? extends RestTokenEntry> iid;
		if(bundleID != null) {
			iid = generateRestTokenEntryBidInstanceIdentifier(bundleID);
		} else {
			iid = generateRestTokenEntryTokenInstanceIdentifier(apiToken);
		}
		
		//Create Transaction and Run
		ReadOnlyTransaction transaction = db.newReadOnlyTransaction();
		CheckedFuture<?, ReadFailedException> future = transaction.read(DS_TYPE, iid);
		Optional<? extends RestTokenEntry> optional = Optional.absent();
        try {
            optional = (Optional<? extends RestTokenEntry>) future.checkedGet();
        } catch (ReadFailedException e) {
            LOG.warn("Reading RestTokenEntry failed:",e);
        }
        
        //Use result
        if(optional.isPresent()) {
        	RestTokenEntry entry = optional.get();
        	return entry;
        } else {
        	LOG.debug("RestTokenEntry Not Found");
        	return null;
        }
	}
	
	/**
	 * Write a token entry associated bundleID and apiToken
	 * Will overwrite an existing bundleID apiToken if one exists
	 */
	private void writeRestTokenEntry(String bundleID, String apiToken) {
		WriteTransaction transaction = db.newWriteOnlyTransaction();
		
		//Write RestTokenEntryBid and RestTokenEntryToken
		InstanceIdentifier<RestTokenEntryBid> iid = generateRestTokenEntryBidInstanceIdentifier(bundleID);
		RestTokenEntryBid entryBid = new RestTokenEntryBidBuilder()
        	.setApiToken(apiToken)
        	.setBundleId(bundleID)
        	.build();
		transaction.put(DS_TYPE, iid, entryBid);
		InstanceIdentifier<RestTokenEntryToken> iid2 = generateRestTokenEntryTokenInstanceIdentifier(apiToken);
		RestTokenEntryToken entryToken = new RestTokenEntryTokenBuilder()
    		.setApiToken(apiToken)
    		.setBundleId(bundleID)
    		.build();
		transaction.put(DS_TYPE, iid2, entryToken);
        
        //Run
        submitAndLogWriteTransaction(transaction, "RestTokenEntry Put");
	}
	
	/**
	 * Delete the token entry for bundleID
	 */
	private void deleteRestTokenEntry(String bundleID) {
		// Read first to get apiToken
		RestTokenEntry rte = readRestTokenEntry(bundleID, null);
		if(rte == null) return;
		
		// Delete both entries
		WriteTransaction transaction = db.newWriteOnlyTransaction();
		InstanceIdentifier<RestTokenEntryBid> iid = generateRestTokenEntryBidInstanceIdentifier(rte.getBundleId());
		InstanceIdentifier<RestTokenEntryToken> iid2 = generateRestTokenEntryTokenInstanceIdentifier(rte.getApiToken());
		transaction.delete(DS_TYPE, iid);
		transaction.delete(DS_TYPE, iid2);
        submitAndLogWriteTransaction(transaction, "RestTokenEntry Delete", true);
	}
	
	//================================================================================
    // Helper Methods
    //================================================================================
	
	/**
	 * This method generates the fallback locations for when read requests attempt to access non-existent 
	 * locations in. The hierarchy of the default locations are as follows: 
	 * 1. Explicit Permission, 2. Identifier Default Permission, 3. Action Default Permission, 4. Global Default Permission
	 */
	private Queue<PermissionLocation> getFallbackLoctions(PermissionLocation loc) {
		Queue<PermissionLocation> ret = new LinkedList<PermissionLocation>();
		PermissionLocation nullComp = new PermissionLocation(null, null);
    	if(!loc.getIdentifier().equals(nullComp.getIdentifier()) && !loc.getAction().equals(nullComp.getAction())) {
        	ret.add(new PermissionLocation(loc.getIdentifier(), null));
        	ret.add(new PermissionLocation(null, loc.getAction()));
    	} 
    	if(!loc.getIdentifier().equals(nullComp.getIdentifier()) | !loc.getAction().equals(nullComp.getAction())) {
        	ret.add(new PermissionLocation(null, null));
    	}
    	return ret;
	}
	
	private InstanceIdentifier<PermissionMapEntry> generatePermissionMapEntryInstanceIdentifier(PermissionLocation loc) {
		return generatePermissionMapEntryInstanceIdentifier(loc.getIdentifier());
	}
	
	private InstanceIdentifier<PermissionMapEntry> generatePermissionMapEntryInstanceIdentifier(String identifier) {
		InstanceIdentifier<PermissionMapEntry> iid = InstanceIdentifier.create(PermissionMap.class)
		        .child(PermissionMapEntry.class, new PermissionMapEntryKey(identifier));
	    return iid;
	}
	
	private InstanceIdentifier<AccessEntry> generateAccessEntryInstanceIdentifier(PermissionLocation loc) {
		InstanceIdentifier<AccessEntry> iid = generatePermissionMapEntryInstanceIdentifier(loc)
            .child(AccessEntry.class, new AccessEntryKey(loc.getAction()));
        return iid;
    }
	
	private InstanceIdentifier<RestTokenEntryBid> generateRestTokenEntryBidInstanceIdentifier(String bundleID) {
		InstanceIdentifier<RestTokenEntryBid> iid = InstanceIdentifier.create(RestTokenTable.class)
					.child(RestTokenEntryBid.class, new RestTokenEntryBidKey(bundleID));
	    return iid;
	}
	
	private InstanceIdentifier<RestTokenEntryToken> generateRestTokenEntryTokenInstanceIdentifier(String token) {
		InstanceIdentifier<RestTokenEntryToken> iid = InstanceIdentifier.create(RestTokenTable.class)
					.child(RestTokenEntryToken.class, new RestTokenEntryTokenKey(token));
	    return iid;
	}
	
	private void submitAndLogWriteTransaction(WriteTransaction transaction, final String transactionLogTag) {
		submitAndLogWriteTransaction(transaction, transactionLogTag, false);
	}
	
	private void submitAndLogWriteTransaction(WriteTransaction transaction, final String transactionLogTag, final boolean mayFail) {
		CheckedFuture<Void, TransactionCommitFailedException> future = transaction.submit();
		
        Futures.addCallback(future, new FutureCallback<Void>() {
            public void onFailure(Throwable e) {
            	if(mayFail) {
            		LOG.debug("Write Transaction Failed! Transaction ID: "+transactionLogTag, e);
            	} else {
            		LOG.warn("Write Transaction Failed! Transaction ID: "+transactionLogTag, e);
            	}
            }
            public void onSuccess(Void arg0) {
                LOG.debug("Write Transaction Success! Transaction ID: "+transactionLogTag, arg0);
            }
        });
        // Make thread wait for write to finish.
        try {
			future.get();
		} catch (InterruptedException | ExecutionException e1) {}
	}
	
	//================================================================================
    // Helper Classes
    //================================================================================
	
	/**
	 * Assists with the parsing of input by automatically changing null values to the default string.
	 */
	private class PermissionLocation {
		private final String identifier;
		public String getIdentifier() {
			return identifier;
		}

		private final String action;
		public String getAction() {
			return action;
		}
		
		public PermissionLocation() {
			this(null, null);
		}
		
		public PermissionLocation(RevokePrivilegeInput input) {
			this(input.getAppId(), input.getAction());
		}
		
		public PermissionLocation(GrantPrivilegeInput input) {
			this(input.getAppId(), input.getAction());
		}
		
		public PermissionLocation(IsAuthorizedInput input) {
			this(input.getAppId(), input.getAction());
		}
		
		public PermissionLocation(String identifier, String action) {
			this.identifier = (identifier != null) ? identifier : DEFAULT_STR;
			this.action = (action != null) ? action : DEFAULT_STR;
		}
	}
}