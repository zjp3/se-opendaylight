/*
 * Copyright © 2015 Copyright (c) 2015 Zachary Podbela, Craig Boswell and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package edu.duke.accesscontrol.it;

import static org.ops4j.pax.exam.CoreOptions.composite;
import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.editConfigurationFilePut;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opendaylight.controller.mdsal.it.base.AbstractMdsalTestBase;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.AccesscontrolService;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GetBundleIdInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GetBundleIdInputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GetBundleIdOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GrantPrivilegeInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.GrantPrivilegeInputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IsAuthorizedInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IsAuthorizedInputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IsAuthorizedOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IssueTokenInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IssueTokenInputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.IssueTokenOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.ResetToDefaultInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.ResetToDefaultInputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RevokePrivilegeInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RevokePrivilegeInputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RevokeTokenInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.accesscontrol.rev150105.RevokeTokenInputBuilder;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.karaf.options.LogLevelOption.LogLevel;
import org.ops4j.pax.exam.options.MavenUrlReference;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class AccessControlIT extends AbstractMdsalTestBase {
    private static final Logger LOG = LoggerFactory.getLogger(AccessControlIT.class);

    @Override
    public String getModuleName() {
        return "accesscontrol";
    }

    @Override
    public String getInstanceName() {
        return "accesscontrol-default";
    }

    @Override
    public MavenUrlReference getFeatureRepo() {
        return maven()
                .groupId("edu.duke.accesscontrol")
                .artifactId("accesscontrol-features")
                .classifier("features")
                .type("xml")
                .versionAsInProject();
    }

    @Override
    public String getFeatureName() {
        return "odl-accesscontrol-ui";
    }

    @Override
    public Option getLoggingOption() {
        Option option = editConfigurationFilePut(ORG_OPS4J_PAX_LOGGING_CFG,
                logConfiguration(AccessControlIT.class),
                LogLevel.INFO.name());
        option = composite(option, super.getLoggingOption());
        return option;
    }

    @Test
    public void testaccesscontrolFeatureLoad() {
        Assert.assertTrue(true);
    }
    
    private static final String TID = "com.pod.hello";
    private static final String action = "eat";
    
    @Test
    public void testIsAuthorized() throws InterruptedException, ExecutionException {
    	AccesscontrolService service = getSession().getRpcService(AccesscontrolService.class);
    	for(int i = 0; i < 4; i++) {
    		String id = (i < 2) ? null : TID;
    		String act = (i % 2 == 0) ? null : action;
            Assert.assertTrue("isAuthorized Failed when inputs are id: "+id+" act: "+act+" and no specific map entries exist (no writes since init).", performIsAuthorized(service, id, act).getResult().isResult());
    	}
    }
    	
    @Test
    public void testWritingDefaults() throws InterruptedException, ExecutionException {
    	AccesscontrolService service = getSession().getRpcService(AccesscontrolService.class);
    	Assert.assertTrue("isAuthorized Failed when inputs are id: "+TID+" act: "+action+" and no specific map entries exist (no writes since init).", performIsAuthorized(service, TID, action).getResult().isResult());
    	performRevokePrivilege(service, null, null);
    	Assert.assertFalse("Global default write failed. Expected false, recieved true.", performIsAuthorized(service, TID, action).getResult().isResult());
    	performGrantPrivilege(service, null, action);
    	Assert.assertTrue("Action default write failed. Expected true, recieved false.", performIsAuthorized(service, TID, action).getResult().isResult());
    	performRevokePrivilege(service, TID, null);
    	Assert.assertFalse("Identifier default write failed. Expected false, recieved true.", performIsAuthorized(service, TID, action).getResult().isResult());
    	performGrantPrivilege(service, TID, action);
    	Assert.assertTrue("Explict permission write failed. Expected true, recieved false.", performIsAuthorized(service, TID, action).getResult().isResult());
    }
    
    @Test
    public void testReset() throws InterruptedException, ExecutionException {
    	AccesscontrolService service = getSession().getRpcService(AccesscontrolService.class);
    	Assert.assertTrue("isAuthorized Failed when inputs are id: "+TID+" act: "+action+" and no specific map entries exist (no writes since init).", performIsAuthorized(service, TID, action).getResult().isResult());
    	performRevokePrivilege(service, TID, action);
    	Assert.assertFalse("Explict permission write failed. Expected false, recieved true.", performIsAuthorized(service, TID, action).getResult().isResult());
    	performResetToDefault(service, TID);
    	Assert.assertTrue("Reset to default failed. Expected global default value true, Recieved explicit permission value false.", performIsAuthorized(service, TID, action).getResult().isResult());
    }
    
    @Test
    public void testTokenManagement() throws InterruptedException, ExecutionException {
    	AccesscontrolService service = getSession().getRpcService(AccesscontrolService.class);
    	String token = performIssueToken(service, TID).getResult().getToken();
    	Assert.assertNotEquals("issueToken failed. No token was returned", token, "");
    	Assert.assertEquals("getBundleID failed.", TID, performGetBundleId(service, token).getResult().getAppId());
    	performRevokeToken(service, TID);
    	Assert.assertNotEquals("getBundleID failed.", TID, performGetBundleId(service, token).getResult().getAppId());
    }
    
    private static RpcResult<IsAuthorizedOutput> performIsAuthorized(AccesscontrolService service, String id, String act) throws InterruptedException, ExecutionException {
    	IsAuthorizedInput input = new IsAuthorizedInputBuilder()
			.setAppId(id)
			.setAction(act)
			.build();
    	Future<RpcResult<IsAuthorizedOutput>> outputFuture = service.isAuthorized(input);
    	return outputFuture.get();
    }
    
    private static RpcResult<Void> performRevokePrivilege(AccesscontrolService service, String id, String act) throws InterruptedException, ExecutionException {
    	RevokePrivilegeInput input = new RevokePrivilegeInputBuilder()
			.setAppId(id)
			.setAction(act)
			.build();
    	Future<RpcResult<Void>> outputFuture = service.revokePrivilege(input);
    	return outputFuture.get();
    }
    
    private static RpcResult<Void> performGrantPrivilege(AccesscontrolService service, String id, String act) throws InterruptedException, ExecutionException {
    	GrantPrivilegeInput input = new GrantPrivilegeInputBuilder()
			.setAppId(id)
			.setAction(act)
			.build();
    	Future<RpcResult<Void>> outputFuture = service.grantPrivilege(input);
    	return outputFuture.get();
    }
    
    private static RpcResult<Void> performResetToDefault(AccesscontrolService service, String id) throws InterruptedException, ExecutionException {
    	ResetToDefaultInput input = new ResetToDefaultInputBuilder()
			.setAppId(id)
			.build();
    	Future<RpcResult<Void>> outputFuture = service.resetToDefault(input);
    	return outputFuture.get();
    }
    
    private static RpcResult<IssueTokenOutput> performIssueToken(AccesscontrolService service, String id) throws InterruptedException, ExecutionException {
    	IssueTokenInput input = new IssueTokenInputBuilder()
			.setAppId(id)
			.build();
    	Future<RpcResult<IssueTokenOutput>> outputFuture = service.issueToken(input);
    	return outputFuture.get();
    }
    
    private static RpcResult<Void> performRevokeToken(AccesscontrolService service, String id) throws InterruptedException, ExecutionException {
    	RevokeTokenInput input = new RevokeTokenInputBuilder()
			.setAppId(id)
			.build();
    	Future<RpcResult<Void>> outputFuture = service.revokeToken(input);
    	return outputFuture.get();
    }
    
    private static RpcResult<GetBundleIdOutput> performGetBundleId(AccesscontrolService service, String token) throws InterruptedException, ExecutionException {
    	GetBundleIdInput input = new GetBundleIdInputBuilder()
			.setToken(token)
			.build();
    	Future<RpcResult<GetBundleIdOutput>> outputFuture = service.getBundleId(input);
    	return outputFuture.get();
    }
}
