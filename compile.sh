#!/bin/bash
# My first script

output_path='..'

while getopts ":o:" opt; do
  case $opt in
    o)
      output_path=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

cd controller
mvn clean install -DskipTests
cd ..
cd distribution
mvn clean install -DskipTests
cd ..
cp ./distribution/distributions/karaf/target/distribution-karaf-0.3.3-SNAPSHOT.tar.gz $output_path